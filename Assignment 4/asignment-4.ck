// Author: Daniel Buendia
// Date: 19/03/16
<<< "Daniel Buendía - Assignment 4" >>>; 

now => time start;

now + 10::second => time later;
while (now < later) {
    <<<"Time left:", (later-now)/second>>>;
    second => now;
}

now + 10::second => time later2;
while (now < later2) {
    <<<"Time left:", (later2-now)/second>>>;
    second => now;    
}

now + 10::second => time later3;
while (now < later) {
    <<<"Time left:", (later3-now)/second>>>;
    second => now;    
}

<<<"Elapsed Time", (now-start)/second>>>;
