// Author: Daniel Buendía
// Date: 12/02/16

<<< "Assignment 1 - Daniel Buendía" >>>;

// Sound Network

SinOsc sin => NRev r => dac;
0.4 => sin.gain;
.4 => r.mix;

0 => int count;
1 => int check;

while (check == 1) {
    //If block that checks if 50 freqs have been played   
    if (count <= 25) {    
        //Chooses randomly very low freqs
        Math.random2f(20, 50) => sin.freq;
        .15::second => now;
        count++;
    } else {
        // Diminuendo with a for loop.
        for (.2 => float i; i >= 0.0; i - 0.002 => i) {
            i => sin.gain;            
            Math.random2f(20, 250) => sin.freq; 
            .1::second => now;      
            <<< count >>>;        
            count++;         
        }     
        0 => check;
    }               
}           