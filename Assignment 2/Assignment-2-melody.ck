// Author: Daniel Buendia
// Date: 22/02/16
<<< "Daniel Buendia - Assignment 2" >>>; 

// Sound Network 
SinOsc mel => dac;

// Note values 
0.5 => float neg; // Quarter note - negra
neg / 2 => float corch; // Eigth note - corchea
corch / 2 => float scorch; // Sixteenth note - semicorchea
(2*neg)/3 => float tres; // Triplet - tresillo de corchea
neg * 2 => float bla; // Half note - Blanca

//Arrays
[48, 50, 51, 55, 57, 55, 57, 53, 57, 55, 53, 55, 51, 51, 55, 53, 50, 46] @=> int Aa[]; // Melody a
[48, 50, 51, 55, 58, 57, 55, 57, 53, 60, 55] @=> int Ab[]; // Melody b
[36, 41, 39, 36, 32, 34] @=> int Abassa[]; // Bass a
[36, 43, 41, 36] @=> int Abassb[]; // Bass b
[43, 46, 48, 45, 43, 43, 41] @=> int Atena[]; // Tenor a
[43, 50, 48, 43] @=> int Atenb[]; // Tenor b

[neg+corch, corch, neg, neg, scorch, scorch, bla+corch, corch, corch, scorch, scorch, neg, corch, neg, neg, bla, neg, neg] @=> float Dura[];
[neg, neg, neg, neg, neg+corch, scorch, scorch, tres, tres, tres, bla*4] @=> float Durb[];

1 => int counter;
if (counter == 1) {
    for (0 => int i; i < Aa.cap(); i++) 
    {
        Std.mtof(Aa[i]) => mel.freq;
        0.2 => mel.gain;
        Dura[i]::second => now;     
    }
    
    for (0 => int i; i < Ab.cap(); i++) 
    {
        Std.mtof(Ab[i]) => mel.freq;
        0.2 => mel.gain;
        Durb[i]::second => now;
        <<< i, Ab[i] >>>;   
    }
        // Noise starts in the right speaker
        Noise n1 => Pan2 p => dac.right;
        0.05 => p.gain;
        10::ms => now;

    for (0 => int i; i < Aa.cap(); i++) 
    {
        Std.mtof(Aa[Math.random2(0, Aa.size()-1)]) => mel.freq;
        0.2 => mel.gain;
        Dura[i]::second => now;     
    }
    
    for (0 => int i; i < Ab.cap(); i++) 
    {
        Std.mtof(Aa[Math.random2(0, Aa.size()-1)]) => mel.freq;
        0.2 => mel.gain;
        Durb[i]::second => now;  
    }
        // Noise starts in the left speaker
        Noise n2 => Pan2 p2 => dac.left;
        0.05 => p2.gain;
        10::ms => now;
        
    for (0 => int i; i < Aa.cap(); i++) 
    {
        Std.mtof(Aa[Math.random2(0, Aa.size()-1)]) => mel.freq;
        Math.randomf() => mel.gain;
        Dura[i]::second => now;     
    }
    
    for (0 => int i; i < Ab.cap(); i++) 
    {
        Std.mtof(Aa[Math.random2(0, Aa.size()-1)]) => mel.freq;
        Math.randomf() => mel.gain;
        Durb[i]::second => now;  
    }
}


