// Author: Daniel Buendia
// Date: 04/03/16
<<< "Daniel Buendia - Assignment 3" >>>; 

// Sound Network 
SawOsc s => NRev r => Pan2 p => dac;
0.9 => s.gain;
.4 => r.mix;

// SndBufs connected to dac

SndBuf cow => dac;
SndBuf kick => dac;
SndBuf clap => dac;

// Read them
me.dir() + "audio/cowbell_01.wav" => cow.read;
me.dir() + "audio/kick_05.wav" => kick.read;
me.dir() + "audio/clap_01.wav" => clap.read;

// Arrays
[30, 49, 52, 54, 57, 59, 61, 66, 69, 71, 73, 76, 78, 81, 83, 85, 88, 90, 93] @=> int fsharp[];
[26, 30, 37, 38, 40, 42, 45, 49, 54, 57, 59, 61, 62, 66, 69, 71, 73, 74, 78] @=> int dM[];


0 => int counter; //for percussion block
0 => int counter2; //used to change from while1 - while2
0 => int counter3; //used to change from while2 - while3

1 => int arp1; //set true.

while (arp1) {
    
    // Moves along the fsharp array until certain random position  
    for (0 => int i; i < Math.random2(0, fsharp.cap()); i++) {
        
        //Percussion Begin
        counter % 9 => int beat;
        
        if ((beat == 0) || (beat == 4)) {
            0 => cow.pos;
            Math.random2f(-1, 1) => cow.rate;
        }
        
        if ((beat == 2) || (beat == 6)) {
            0 => kick.pos;
            Math.random2f(-1, 4) => kick.rate;
        }
        
        0 => clap.pos;
        Math.random2f(0.5, 23) => clap.rate;
        counter++;
        // Percussion End
         
        //Arpeggio fsharp Begin
        Std.mtof(fsharp[i]) => s.freq;
        0.2 => s.gain;
        .2::second => now; 
        // Arpeggio fsharp End 
        
        0 => arp1;
        
       
    }
    
    while (!arp1) {
        // Moves along the dM array until certain random position   
        for (0 => int i; i < Math.random2(0, dM.cap()); i++) {
            
            //Percussion Begin
            counter % 9 => int beat;
            
            if ((beat == 0) || (beat == 4)) {
                0 => cow.pos;
                Math.random2f(.6, 1.4) => cow.rate;
            }
            
            if ((beat == 2) || (beat == 6)) {
                0 => kick.pos;
                Math.random2f(.6, 4) => kick.rate;
            }
            
            0 => clap.pos;
            Math.random2f(1, 23) => clap.rate;
            counter++;
            //Percussion End
            
            // Arpeggio dM Begin
            Std.mtof(dM[i]) => s.freq;
            0.2 => s.gain;
            .2::second => now;
            // Arpeggio dM End     
             
            // Returns to while1
            1 => arp1;
            counter2++;
            
            if (counter2 > 20) {                
                //Goes to while3
                2 => arp1; 
            }                 
        }
    } 
    
    while (arp1 == 2) {
        
        for (0 => int i; i < Math.random2(0, fsharp.cap()); i++) {
            
            // Percussion Begin
            counter % 9 => int beat;
            
            if ((beat == 0) || (beat == 4)) {
                0 => cow.pos;
                Math.random2f(.6, 1.4) => cow.rate;
            }
            
            if ((beat == 2) || (beat == 6)) {
                0 => kick.pos;
                Math.random2f(.6, 4) => kick.rate;
            }
            
            0 => clap.pos;
            Math.random2f(1, 23) => clap.rate;
            counter++;
            // Percussion End
            
            // Arpeggio fsharp Begin
            Std.mtof(fsharp[i]) => s.freq;
            0.2 => s.gain;
            .2::second => now;             
            //Set different panning possibilities
            Math.random2f(-1, 1) => p.pan;
            // Arpeggio fsharp End
            
            //Arpeggio dM Begin
            Std.mtof(dM[i]) => s.freq;
            0.2 => s.gain;
            .2::second => now;
              Math.random2f(-1, 1) => p.pan;
            // Arpeggio dM End
            
            1 => arp1;
        }
    }    
} 
